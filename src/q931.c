/* q931.c - Q.931 protocol definitions
 *
 * (C) 2022 by Harald Welte <laforge@osmocom.org>
 *
 * All Rights Reserved
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <osmocom/core/utils.h>
#include "q931.h"

const struct value_string q931_msg_type_vals[] = {
	{ Q931_MSGT_ALERTING,		"ALERTING" },
	{ Q931_MSGT_CALL_PROCEEDING,	"CALL PROCEEDING" },
	{ Q931_MSGT_CONNECT,		"CONNECT" },
	{ Q931_MSGT_CONNECT_ACK,	"CONNECT ACK" },
	{ Q931_MSGT_PROGRESS,		"PROGRESS" },
	{ Q931_MSGT_SETUP,		"SETUP" },
	{ Q931_MSGT_SETUP_ACK,		"SETUP ACK" },
	{ Q931_MSGT_RESUME,		"RESUME" },
	{ Q931_MSGT_RESUME_ACK,		"RESUME ACK" },
	{ Q931_MSGT_RESUME_REJ,		"RESUME REJECT" },
	{ Q931_MSGT_SUSPEND,		"SUSPEND" },
	{ Q931_MSGT_SUSPEND_ACK,	"SUSPEND ACK" },
	{ Q931_MSGT_SUSPEND_REJ,	"SUSPEND REJECT" },
	{ Q931_MSGT_USER_INFO,		"USER INFO" },
	{ Q931_MSGT_DISCONNECT,		"DISCONNECT" },
	{ Q931_MSGT_RELEASE,		"RELEASE" },
	{ Q931_MSGT_RELEASE_COMPLETE,	"RELEASE COMPLETE" },
	{ Q931_MSGT_RESTART,		"RESTART" },
	{ Q931_MSGT_RESTART_ACK,	"RESTART ACK" },
	{ Q931_MSGT_SEGMENT,		"SEGMENT" },
	{ Q931_MSGT_CONGESTION_CTRL,	"CONGESTION CTRL" },
	{ Q931_MSGT_INFORMATION,	"INFORMATION" },
	{ Q931_MSGT_NOTIFY,		"NOTIFY" },
	{ Q931_MSGT_STATUS,		"STATUS" },
	{ Q931_MSGT_STATUS_ENQIURY,	"STATUS ENQUIRY" },
	{ 0, NULL }
};
