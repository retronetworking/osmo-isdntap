/*
 * osmo-isdntap.c
 *
 * (C) 2022 by Harald Welte <laforge@osmocom.org>
 *
 * All Rights Reserved
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <errno.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>

#define _GNU_SOURCE
#include <getopt.h>

#include <talloc.h>

#include <osmocom/core/application.h>
#include <osmocom/core/msgb.h>
#include <osmocom/core/select.h>
#include <osmocom/core/stats.h>
#include <osmocom/vty/telnet_interface.h>
#include <osmocom/vty/logging.h>
#include <osmocom/vty/stats.h>
#include <osmocom/vty/misc.h>
#include <osmocom/vty/cpu_sched_vty.h>

#include "isdntap.h"
#include "log.h"

#ifndef OSMO_VTY_PORT_ISDNTAP
#define OSMO_VTY_PORT_ISDNTAP	4270
#endif

static const char *g_config_file = "osmo-isdntap.cfg";
static void *g_itap_ctx = NULL;
static int g_shutdown = 0;


static void sig_handler(int signo)
{
	fprintf(stdout, "signal %d received\n", signo);
	switch (signo) {
	case SIGINT:
	case SIGTERM:
		fprintf(stdout, "shutting down\n");
		g_shutdown = 1;
		break;
	case SIGABRT:
	case SIGUSR1:
		talloc_report(g_itap_ctx, stderr);
		talloc_report_full(g_itap_ctx, stderr);
		break;
	case SIGUSR2:
		talloc_report_full(g_itap_ctx, stderr);
		break;
	default:
		break;
	}
}

static struct vty_app_info vty_info = {
	.name = "osmo-isdntap",
	.version = PACKAGE_VERSION,
	.copyright =
	"(C) 2022 by Harald Welte and contributors\r\n",
	"License GPLv2+: GNU GPL version 2 or later <http://gnu.org/licenses/gpl-2.0.html>\r\n"
	"This is free software: you are free to change and redistribute it.\r\n"
	"There is NO WARRANTY, to the extent permitted by law.\r\n",
};

static void print_help(void)
{
	printf("  Some useful help...\n");
	printf("  -h --help			This text.\n");
	printf("  -d --debug option		--debug=DITAP:DQ931 enable debugging.\n");
	printf("  -c --config-file filename	The config file to use.\n");
}

static void handle_options(int argc, char **argv)
{
	while (1) {
		int option_index = 0, c;
		static const struct option long_options[] = {
			{"help", 0, 0, 'h'},
			{"debug", 1, 0, 'd'},
			{"config-file", 1, 0, 'c'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "hd:c:", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			print_help();
			exit(0);
			break;
		case 'd':
			log_parse_category_mask(osmo_stderr_target, optarg);
			break;
		case 'c':
			g_config_file = optarg;
			break;
		default:
			fprintf(stderr, "Error in command line options. Exiting.\n");
			exit(-1);
		}
	}

	if (argc > optind) {
		fprintf(stderr, "Unsupported positional arguments on command line\n");
		exit(2);
	}
}

int main(int argc, char *argv[])
{
	struct isdntap_instance *itd = NULL;
	int rv;

	/* talloc init */
	g_itap_ctx = talloc_named_const(NULL, 0, "osmo-isdntap");
	msgb_talloc_ctx_init(g_itap_ctx, 0);
	vty_info.tall_ctx = g_itap_ctx;

	/* logging init */
	osmo_init_logging2(g_itap_ctx, &log_info);

	/* signals init */
	signal(SIGINT, &sig_handler);
	signal(SIGTERM, &sig_handler);
	signal(SIGABRT, &sig_handler);
	signal(SIGUSR1, &sig_handler);
	signal(SIGUSR2, &sig_handler);
	osmo_init_ignore_signals();

	/* main state */
	itd = talloc_zero(g_itap_ctx, struct isdntap_instance);
	OSMO_ASSERT(itd);
	INIT_LLIST_HEAD(&itd->lines);
	itd->cfg.output_path = talloc_strdup(itd, "/tmp");

	vty_init(&vty_info);
	logging_vty_add_cmds();
	isdntap_vty_init(itd);
	rate_ctr_init(itd);
	osmo_stats_init(itd);
	osmo_stat_item_init(itd);
	osmo_stats_vty_add_cmds();
	osmo_talloc_vty_add_cmds();
	osmo_fsm_vty_add_cmds();
	osmo_cpu_sched_vty_init(itd);

	handle_options(argc, argv);

	rv = vty_read_config_file(g_config_file, NULL);
	if (rv < 0) {
		LOGP(DITAP, LOGL_FATAL, "Failed to parse the config file '%s'\n", g_config_file);
		exit(2);
	}

	rv = telnet_init_dynif(g_itap_ctx, itd, vty_get_bind_addr(), OSMO_VTY_PORT_ISDNTAP);
	if (rv != 0) {
		LOGP(DITAP, LOGL_FATAL, "Failed to bind VTY interface to %s:%u\n",
			vty_get_bind_addr(), OSMO_VTY_PORT_ISDNTAP);
		exit(1);
	}

	/* main loop */
	while (!g_shutdown) {
		osmo_select_main(0);
	}

	talloc_free(itd);

	return 0;
}
