osmo-isdntap - Osmocom ISDN tap daemon
======================================

This repository contains a small stand-alone [daemon] program that you can run to
create recordings of ISDN calls.

It is part of the [Osmocom retronetworking](https://retronetworking.org/) project.

The program works by passively decoding the ISDN D-channel, specifically the Q.931
call control protocol.  Whenever a call is being set up, it starts to capture the B-channel
timeslot allocated to that call.  It creates two files, one for each of the two directions
of the call.  The files contain the raw transparent binary B-channel data.

Currently the only supported ISDN hardware/driver interface is DAHDI.

Currently, only PRI (primary rate) ISDN interfaces are supported.

Homepage
--------

https://osmocom.org/projects/osmo-isdntap/wiki

GIT Repository
--------------

You can clone from the official osmo-isdntap.git repository using

	git clone https://gitea.osmocom.org/retronetworking/osmo-isdntap

There is a web interface at <https://gitea.osmocom.org/retronetworking/osmo-isdntap>


Converting recordings to audio
------------------------------

If you'd like to convert any of the recorded files to audio, you can use the following example
command to do so:

	sox --channels 1 --type raw --rate 8000 -e a-law isdntap-03012342111-03012343038-20221016-212558-8-tx.raw output.wav

The generated `output.wav` file can then be played back with any standard audio player.
