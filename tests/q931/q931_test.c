
#include <osmocom/gsm/tlv.h>
#include "q931.h"

static void test_setup(void)
{
	const uint8_t msg[] = {
		0x08, 0x02, 0x00, 0x23, 0x05, 0x04, 0x03, 0x90,
		0x90, 0xa3, 0x18, 0x03, 0xa1, 0x83, 0x81, 0x6c,
		0x0d, 0x41, 0x80, 0x30, 0x33, 0x30, 0x31, 0x32,
		0x33, 0x34, 0x32, 0x31, 0x31, 0x31
	};
	struct q931_msg_parsed q931;
	int rc;

	memset(&q931, 0, sizeof(q931));
	rc = q931_msg_parse(&q931, msg, sizeof(msg));

	OSMO_ASSERT(rc == 0);
	OSMO_ASSERT(q931.msg_type == Q931_MSGT_SETUP);
	OSMO_ASSERT(q931.call_ref == 0x0023);
	OSMO_ASSERT(TLVP_PRES_LEN(&q931.ies, Q931_IEI_BEARER_CAP, 3));
	OSMO_ASSERT(TLVP_PRES_LEN(&q931.ies, Q931_IEI_CHANNEL_ID, 3));
	OSMO_ASSERT(TLVP_PRES_LEN(&q931.ies, Q931_IEI_CALLING_PARTY_NUM, 13));
}

static void test_setup_ack(void)
{
	const uint8_t msg[] = {
		0x08, 0x02, 0x80, 0x24, 0x0d, 0x18, 0x03,
		0xa9, 0x83, 0x81
	};
	struct q931_msg_parsed q931;
	int rc;

	memset(&q931, 0, sizeof(q931));
	rc = q931_msg_parse(&q931, msg, sizeof(msg));

	OSMO_ASSERT(rc == 0);
	OSMO_ASSERT(q931.msg_type == Q931_MSGT_SETUP_ACK);
	OSMO_ASSERT(q931.call_ref == 0x80000024);
	OSMO_ASSERT(TLVP_PRES_LEN(&q931.ies, Q931_IEI_CHANNEL_ID, 3));
}

static void test_information(void)
{
	const uint8_t msg[] = {
		0x08, 0x02, 0x00, 0x24, 0x7b, 0x70, 0x05,
		0x81, 0x33, 0x30, 0x33, 0x38
	};
	struct q931_msg_parsed q931;
	int rc;

	memset(&q931, 0, sizeof(q931));
	rc = q931_msg_parse(&q931, msg, sizeof(msg));

	OSMO_ASSERT(rc == 0);
	OSMO_ASSERT(q931.msg_type == Q931_MSGT_INFORMATION);
	OSMO_ASSERT(q931.call_ref == 0x00000024);
	OSMO_ASSERT(TLVP_PRES_LEN(&q931.ies, Q931_IEI_CALLED_PARTY_NUM, 5));
}

static void test_alerting(void)
{
	const uint8_t msg[] = {
		0x08, 0x02, 0x80, 0x24, 0x01, 0x1e, 0x02,
		0x81, 0x88
	};
	struct q931_msg_parsed q931;
	int rc;

	memset(&q931, 0, sizeof(q931));
	rc = q931_msg_parse(&q931, msg, sizeof(msg));

	OSMO_ASSERT(rc == 0);
	OSMO_ASSERT(q931.msg_type == Q931_MSGT_ALERTING);
	OSMO_ASSERT(q931.call_ref == 0x80000024);
	OSMO_ASSERT(TLVP_PRES_LEN(&q931.ies, Q931_IEI_PROGRESS_IND, 2));
}


int main(int argc, char **argv)
{
	test_setup();
	test_setup_ack();
	test_information();
	test_alerting();
}
